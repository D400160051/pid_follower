#include "Arduino.h"
#include <Wire.h>
#include "setting.h"
#include "U8glib.h"
U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_DEV_0|U8G_I2C_OPT_NO_ACK|U8G_I2C_OPT_FAST);

#include <EEPROM.h>
#include <Wire.h>

const byte start_addres=5;
byte timerun,index,kabur,cp;
const byte maze=1,kdgain=50,kpgain=13,line_pwm=255;
int cacah,time_lop;

#define ldir         4
#define lpwm_pin     5
#define rdir         7
#define rpwm_pin     6
#define buzzer       8
#define LS           13
#define RS           10

//tomol

const byte s1=A0;//start
const byte s2=A1;//upr
const byte s3=A2;//dwnr
const byte s4=A7;//tab
const byte s5=A6;//upl
const byte s6=A3;//dwnl

//==================================== variable ==========================================
int peka[15],mripat;
int er,pv,salp,sald,lastcild,lpwm,rpwm,indexs;
int sensor_pin[10]={A6,A3,A2,A1,A0,A0,A1,A2,A3,A6};
const byte barlow[]   U8G_PROGMEM={0x00,0x00};
signed char x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,xx1,xx2,xx3,xx8,xx9,xx10;
const byte barhigh[]   U8G_PROGMEM={
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff,
 0xff,0xff
 };
void tampil()
{
 u8g.setRot180();
 u8g.firstPage();
  do{
    //u8g.drawBitmapP(0,0,8,64,w2k);
   u8g.drawStr(40,30,"OS 1.2");
  }
  while(u8g.nextPage());
  delay(2000);
  }
void switching(int a,int b){
digitalWrite(LS,a);
digitalWrite(RS,b);
}
boolean star(){
  boolean data=1;
  if(analogRead(s1)<5)data=0;
  else data=1;
  return data;
}
boolean upr(){
  boolean data=1;
  if(analogRead(s2)<5)data=0;
  else data=1;
  return data;
}
boolean dwnr(){
  boolean data=1;
  if(analogRead(s3)<5)data=0;
  else data=1;
  return data;
}
boolean tab(){
  boolean data=1;
  if(analogRead(s4)<5)data=0;
  else data=1;
  return data;
}
boolean upl(){
  boolean data=1;
  if(analogRead(s5)<5)data=0;
  else data=1;
  return data;
}
boolean dwnl(){
  boolean data=1;
  if(analogRead(s6)<5)data=0;
  else data=1;
  return data;
}

void io(){
Wire.begin();
Serial.begin(9600);
u8g.setRot180();

pinMode(star,INPUT_PULLUP);
pinMode(upr,INPUT_PULLUP);
pinMode(dwnr,INPUT_PULLUP);
pinMode(tab,INPUT_PULLUP);
pinMode(upl,INPUT_PULLUP);
pinMode(dwnl,INPUT_PULLUP);
digitalWrite(star,HIGH);
digitalWrite(upr,HIGH);
digitalWrite(dwnr,HIGH);
digitalWrite(tab,HIGH);
digitalWrite(upl,HIGH);
digitalWrite(dwnl,HIGH);

pinMode(LS,OUTPUT);
pinMode(RS,OUTPUT);
digitalWrite(LS,LOW);
digitalWrite(RS,LOW);

pinMode( ldir, OUTPUT);
pinMode( lpwm_pin, OUTPUT);
pinMode( rpwm_pin, OUTPUT);
pinMode( rdir, OUTPUT);
digitalWrite( ldir, LOW);
digitalWrite( lpwm_pin, LOW);
digitalWrite( rpwm_pin, LOW);
digitalWrite( rdir, LOW);
pinMode(switching,OUTPUT);
digitalWrite(switching,HIGH);

pinMode(buzzer,OUTPUT);
digitalWrite(buzzer,LOW);

ASSR=0x00;
TCCR2A=0x00;
TCCR2B=0x07;
TCNT2=0x00;
OCR2A=0x00;
OCR2B=0x00;
TIMSK2=0x02;
cacah=time_lop=index=0;
syart=0;
cp=0;
u8g.setFont(u8g_font_ncenB10);
tampil();
}



//===================================================
void save_sensor(){
for(int c=0;c<15;c++){
 EEPROM.write(start_addres+c,peka[c]);} 
}

void baca_sens(){
 for(int c=0;c<15;c++){
peka[c] = EEPROM.read(start_addres+c);} 
} 

//======================================================

unsigned char read_adc(unsigned char adc_input)
{
byte data=map(analogRead(adc_input),0,1023,0,255);
return data;
}
void bar_lcd(unsigned char input,unsigned char x){
if(input==0){u8g.drawBitmapP(x,33,1,1,barlow);}
else{u8g.drawBitmapP(x,20,1,13,barhigh);}
} 
 
void scansensor(){
int in=0,x;
for(int adc=0;adc<5;adc++){
switching(1,0);
in=read_adc(sensor_pin[adc]);    
x=adc;
if(adc==0){if(in>peka[adc]){bar_lcd(1,x*9);}
else{bar_lcd(0,x*9);}
}
if(adc==1){if(in>peka[adc]){bar_lcd(1,x*9);}
else{bar_lcd(0,x*9);}}
if(adc==2){if(in>peka[adc]){bar_lcd(1,x*9);}
else{bar_lcd(0,x*9);}}
if(adc==3){if(in>peka[adc]){bar_lcd(1,x*9);}
else{bar_lcd(0,x*9);}}
if(adc==4){if(in>peka[adc]){bar_lcd(1,x*9);}
else{bar_lcd(0,x*9);}}

}//LL   

for(int adc=5;adc<10;adc++){//RR
switching(0,1);  
in=read_adc(sensor_pin[adc]);    
x=adc;
if(adc==5){if(in>peka[adc]){bar_lcd(1,x*9);}//6
else{bar_lcd(0,x*9);}}
if(adc==6){if(in>peka[adc]){bar_lcd(1,x*9);}//7
else{bar_lcd(0,x*9);}}
if(adc==7){if(in>peka[adc]){bar_lcd(1,x*9);}//8
else{bar_lcd(0,x*9);}}
if(adc==8){if(in>peka[adc]){bar_lcd(1,x*9);}//9
else{bar_lcd(0,x*9);}}
if(adc==9){if(in>peka[adc]){bar_lcd(1,x*9);}//10
else{bar_lcd(0,x*9);}}
}
}
// void aut(unsigned char kecc,unsigned int k,unsigned int d){
// er=0-pv;
// salp=(int)er*k;
// sald=(int)(er-lastcild)*d;
// lastcild=er;
// rpwm=(int)kecc+salp+sald;
// lpwm=(int)kecc-salp-sald;
// }
// unsigned int read_sensor(){
// unsigned int input=0,x=0;
// int pecah=0;
// x1=0;x2=0;x3=0;x4=0;x5=0;x6=0;
// x7=0;x8=0;x9=0;x10=0;
// int in=0;
// for(int adc=0;adc<14;adc++){
//   switching(1,0);
// in=read_adc(sensor_pin[adc]);     
// if(adc==0){if(in>peka[adc]){pecah=pecah+1;x1=1;}}
// if(adc==1){if(in>peka[adc]){pecah=pecah+2;x2=1;}}
// if(adc==2){if(in>peka[adc]){pecah=pecah+4;x3=1;}}
// if(adc==3){if(in>peka[adc]){pecah=pecah+8;x4=1;}}
// if(adc==4){if(in>peka[adc]){pecah=pecah+16;x5=1;}}
// delayMicroseconds(80);
// }
// for(int adc=5;adc<10;adc++){
//   switching(0,1);
// in=read_adc(sensor_pin[adc]);     
// if(adc==5){if(in>peka[adc]){pecah=pecah+32;x6=1;}}
// if(adc==6){if(in>peka[adc]){pecah=pecah+64;x7=1;}}
// if(adc==7){if(in>peka[adc]){pecah=pecah+128;x8=1;}}  
// if(adc==8){if(in>peka[adc]){pecah=pecah+256;x9=1;}}
// if(adc==9){if(in>peka[adc]){pecah=pecah+512;x10=1;}}
// delayMicroseconds(80);
// }
// mripat=pecah;
// xx1=x1;
// xx2=x2;
// xx3=x3;
// xx8=x8;
// xx9=x9;
// xx10=x10;
// return pecah;
// }
// void error_in(byte color){
// int byte_byte=read_sensor();
// if(color==1){byte_byte=1023-mripat;
// x1=1-xx1;
// x2=1-xx2;
// x3=1-xx3;
// x8=1-xx8;
// x9=1-xx9;
// x10=1-xx10;
// }
// switch(byte_byte){
// case 0b0000000001: pv=-18; break;
// case 0b0000000011: pv=-15; break;
// case 0b0000000010: pv=-10; break;
// case 0b0000000110: pv=-9; break;
// case 0b0000000100: pv=-8; break;
// case 0b0000001100: pv=-4; break;
// case 0b0000001000: pv=-3; break;
// case 0b0000011000: pv=-2; break;
// case 0b0000010000: pv=-1; break;

// case 0b0000110000: pv=0; break;

// case 0b0000100000: pv=1; break;
// case 0b0001100000: pv=2; break;
// case 0b0001000000: pv=3; break;
// case 0b0011000000: pv=4; break;
// case 0b0010000000: pv=8; break;
// case 0b0110000000: pv=9; break;
// case 0b0100000000: pv=10; break;
// case 0b1100000000: pv=15; break;
// case 0b1000000000: pv=18; break;
 
// }
// }
void motor(int LPWM,int RPWM,int MAXPWM){
  if(MAXPWM>255)MAXPWM=255;
  if(MAXPWM<0)MAXPWM=0;
  if(LPWM>MAXPWM)LPWM=MAXPWM;
  if(LPWM<-MAXPWM)LPWM=-MAXPWM;
  if(RPWM>MAXPWM)RPWM=MAXPWM;
  if(RPWM<-MAXPWM)RPWM=-MAXPWM;
     
if(LPWM==0){ 
analogWrite(lpwm_pin,0); 
digitalWrite(ldir, LOW); 
}
if(LPWM>0){ 
analogWrite(lpwm_pin,LPWM); 
digitalWrite(ldir, LOW); 
}
if(LPWM<0){ 
analogWrite(lpwm_pin,+LPWM); 
digitalWrite(ldir, HIGH); 
}

if(RPWM==0){ 
analogWrite(rpwm_pin,0); 
digitalWrite(rdir, LOW); 
}
if(RPWM>0){ 
analogWrite(rpwm_pin,RPWM);
digitalWrite(rdir, LOW); 
}
if(RPWM<0){ 
analogWrite(rpwm_pin,+RPWM); 
digitalWrite(rdir, HIGH); 
}
}


void sensor_calibrate(int del){
int i,delcek=0;
uint8_t h;
unsigned char high[16],low[16],adc_input;
for(i=0;i<16;i++){
        peka[i]=0;
        low[i]=255;
        high[i]=0;
        }
u8g.firstPage();
    do{
     h = u8g.getFontAscent()-u8g.getFontDescent();
     u8g.setFont(u8g_font_ncenB10);
     u8g.drawStr(10,4*h,"Calibrating..");
    }
    while(u8g.nextPage());
    
while(1){
for(i=0;i<5;i++){
  switching(0,1);
adc_input=read_adc(sensor_pin[i]);   

if(adc_input<low[i]){
low[i]=adc_input;           
}
if(adc_input>high[i]){
high[i]=adc_input;        
}
}
for(i=5;i<10;i++){
  switching(1,0);
adc_input=read_adc(sensor_pin[i]);   

if(adc_input<low[i]){
low[i]=adc_input;           
}
if(adc_input>high[i]){
high[i]=adc_input;        
}
}
delayMicroseconds(10);
for(int b=0;b<16;b++){
peka[b]=(high[b]-low[b])/2+low[b];
}
delcek++;   
if(delcek>=del){
save_sensor();
break;
}
}
}

void bersih(){
er=0;
pv=0;
lastcild=0;
motor(0,0,0);
digitalWrite(buzzer,HIGH);
delay(50);
digitalWrite(buzzer,LOW);
}

// void jalan(byte kecepatan1,byte kp,byte kd,byte linex,byte pwms){
// error_in(line[index]);
// aut(kecepatan1,kp,kd);
// if(lpwm>255)lpwm=255;
// if(rpwm>255)rpwm=255;
// if(lpwm<-255)lpwm=-255;
// if(rpwm<-255)rpwm=-255;
// motor(lpwm,rpwm,pwms);
// }
// int stop_index(int dat){
//   indexs=dat;
//  return indexs; 
// }
// void loop_stop(){
// uint8_t h;
// timerun=0;
// cacah=0;
// time_lop=0;
// lpwm=0;
// rpwm=0;
// motor(0,0,0);
// while(1){
// u8g.firstPage(); 
// do{
//    h = u8g.getFontAscent()-u8g.getFontDescent();
//   u8g.setPrintPos(0,2*h);
//   u8g.print("MISI BERHASIL");
//   u8g.setPrintPos(0,3*h);
//   u8g.print("ALHAMDULILLAH");
// }
// while(u8g.nextPage());
// }
// }
// void turnL(){
//  int vll,vrr;
//   motor(0,0,0);
//   delay(tunda[index]);
// vll=vl[index];vrr=vr[index];
// motor(-vll,vrr,line_pwm);
// int del2=dels[index]*10;
// delay(del2);
// pv=2;
// while(1){
// error_in(line[index-1]);
// if(pv<0)break;  
// }
// bersih();  
// }
// void turnR(){
//  int vll,vrr;
//   motor(0,0,0);
//   delay(tunda[index]);
// vll=vl[index];vrr=vr[index];
// motor(vll,-vrr,line_pwm);
// int del2=dels[index]*10;
// delay(del2);
// pv=-2;
// while(1){
// error_in(line[index-1]);
// if(pv>0)break;  
// }
// bersih();  
// }

// void run_program(){
// if(time_lop<timer[index]){timerun=1;speedgo=speed1[index];//digitalWrite(led_timer,HIGH);
// }
// else {
// timerun=0;speedgo=speed2[index];//digitalWrite(led_timer,LOW);
//  }

// jalan(speedgo,kp[index],kdgain,line[index-1],255);

// if(sensor[index+1]==3){// prempatan/ T
// if(x1==1||x2==1||x3==1&&x8==1||x9==1||x10==1&&time_lop>=timer[index]){syart=1;
// index++;
// }
// }
// if(sensor[index+1]==4){// or kiri
// if(x1==1&&time_lop>=timer[index]){syart=1;
// index++;
// }
// }
// if(sensor[index+1]==5){ // or kanan
// if(x10==1&&time_lop>=timer[index]){syart=1;
// index++;
// }
// }
// if(sensor[index+1]==6){// bleng
// if(mripat==0&&time_lop>=timer[index]){syart=1;
// index++;
// }
// }
// if(sensor[index+1]==7){// semua sensor kena garis
// if(time_lop>=timer[index]){syart=1;
// index++;
// }
// }
// if(index==indexs&&time_lop>=timer){
// motor(0,0,0);
// bersih();
// loop_stop();
// cacah=0;
// time_lop=0;syart;  
// }
// if(syart==1){
// if(aksi[index]==0){
// cacah=0;
// time_lop=0;syart=0;
// }
// if(aksi[index]==1){
// turnL();
// cacah=0;
// time_lop=0;syart=0;
// }
// if(aksi[index]==2){
//  turnR();
// cacah=0;
// time_lop=0;syart=0;
// }
// if(aksi[index]==3){
//   int vll,vrr;
// vll=vl[index];vrr=vr[index];
// motor(vll,vrr,line_pwm);
// int del2=dels[index]*10;
// delay(del2);
// bersih();
// cacah=0;
// time_lop=0;syart=0;
// }
// if(aksi[index]==4){
// int vll,vrr;
// vll=vl[index];vrr=vr[index];
// motor(-vll,-vrr,line_pwm);
// int del2=dels[index]*10;
// delay(del2);
// bersih();
// cacah=0;
// time_lop=0;syart=0;
// }
// if(aksi[index]==5){
// motor(0,0,0);
// bersih();
// loop_stop();
// cacah=0;
// time_lop=0;syart=0;
// }

// }

// }


// ISR(TIMER2_COMPA_vect) {
// if(timerun==1)cacah++;
// if(cacah>=5){time_lop++;cacah=0;}
// }


// void file(){
// //============================SETTING=============================================
// // kecepatan1 kecepatan2 aksi sensor delay timer kp kecepatanbelokL kecepatanbelokR warna garis brake index/plan
// setting(200,150,not_set,not_set,10,10,10,150,150,black,10,0);//index 0
// setting(200,150,right,LL,10,3,kpgain,150,100,black,10,1);//index 1
// setting(200,150,left,LL,10,1,kpgain,150,100,black,10,2);//index 2
// setting(200,150,left,LL,10,5,kpgain,200,150,black,10,3);//index 3
// setting(200,150,left,LL,10,3,kpgain,200,150,black,10,4);//index 4
// setting(230,100,left,LL,15,12,kpgain,200,150,black,10,5);//index 5
// setting(200,100,stop,bleng,10,0,kpgain,200,150,black,10,6);//index 6
// //================================================================================
// }

void home(){
  int timescan=2000;
uint8_t h;
while(1){
u8g.firstPage(); 
do{
   h = u8g.getFontAscent()-u8g.getFontDescent();
  u8g.setPrintPos(5,1*h);
//  u8g.print("CP : ");
//  u8g.print(cp);
  u8g.setPrintPos(0,40*h);
  u8g.print("NHD-ROBOTIC");
  scansensor();
}
while(u8g.nextPage());
if(tab()==0){sensor_calibrate(timescan);}
if(upr()==0){delay(50);cp++;}
if(dwnr()==0){delay(50);cp--;}
if(cp<1){cp=10;}
if(cp>10){cp=1;}
if(star()==0){
 u8g.firstPage();
do{
   h = u8g.getFontAscent()-u8g.getFontDescent();
  u8g.setPrintPos(10,2*h);
  u8g.print("IS RUNING..");
}
while(u8g.nextPage()); 
delay(50);
break;
}

}
// file();
// index=0;
// if(cp>0){
// index=read_index[cp];
// timer[index]=read_tmr[cp];
// }
}